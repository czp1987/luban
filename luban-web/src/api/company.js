import service from '@/utils/request'
// @Tags api
// @Summary 分页获取公司列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /company/getCompanyList [post]
// {
//  page     int
//	pageSize int
// }
export const getCompanyList = (params) => {
  return service({
    url: "/company/getCompanyList",
    method: 'get',
    params
  })
}

// @Tags Api
// @Summary 更新公司
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.UpdateCompanyParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /company/updateCompany [post]
export const updateCompany = (data) => {
  return service({
    url: "/company/updateCompany",
    method: 'put',
    data
  })
}


// @Tags Api
// @Summary 新增公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateCompanyParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /company/CreateCompany [post]

export const createCompany = (data) => {
  return service({
    url: "/company/createCompany",
    method: 'post',
    data
  })
}


// @Tags Api
// @Summary 删除公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteCompanyParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /company/deleteCompany [post]
export const deleteCompany = (data) => {
  return service({
    url: "/company/deleteCompany",
    method: 'delete',
    data
  })
}

// @Tags Api
// @Summary 根据Id查询公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateCompanyParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /company/getCompanyById[get]

export const getCompanyById = (params) => {
  return service({
    url: "/company/getCompanyById",
    method: 'get',
    params
  })
}


// @Tags Api
// @Summary 根据Id查询公司信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.getCompany true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /company/getCompany[get]

export const getCompany = () => {
  return service({
    url: "/company/getCompany",
    method: 'get',
  })
}
