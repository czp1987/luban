import service from '@/utils/request'
// @Tags api
// @Summary 分页获取基础配置列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页获取基础配置列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /basic/getBasicConfigureList [post]
// {
//  page     int
//	pageSize int
// }
export const getBasicConfigureList = (params) => {
    return service({
        url: "/basic/getBasicConfigureList",
        method: 'get',
        params
    })
}
// @Tags Api
// @Summary 新增基础配置
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.createDepartmentParams true "新增基础配置"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /basic/createBasicConfigure[post]

export const createBasicConfigure = (data) => {
    return service({
        url: "/basic/createBasicConfigure",
        method: 'post',
        data
    })
}

// @Tags SysUser
// @Summary 删除基础配置
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SetUserAuth true "删除基础配置"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"修改成功"}"
// @Router /basic/deleteBasicConfigure [delete]
export const deleteBasicConfigure  = (data) => {
    return service({
        url: "/basic/deleteBasicConfigure",
        method: 'delete',
        data: data
    })
}



// @Tags Api
// @Summary 更新基础配置
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.updateDepartment  true "更新基础配置"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /basic/updateBasicConfigure[put]

export const updateBasicConfigure = (data) => {
    return service({
        url: "/basic/updateBasicConfigure",
        method: 'put',
        data: data
    })
}




