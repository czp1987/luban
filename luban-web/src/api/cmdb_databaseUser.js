import service from '@/utils/request'
// @Tags api
// @Summary 分页获取数据库用户列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBDatabase.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/databaseUser/getDatabaseUserList [Get]
// {
//  page     int
//	pageSize int
// }
export const getDatabaseUserList = (params) => {
    return service({
        url: "/cmdb/databaseUser/getDatabaseUserList",
        method: 'get',
        params
    })
}

// @Tags Api
// @Summary 更新数据库用户信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.UpdateDatabaseUserParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/databaseUser/updateDatabaseUser [post]
export const updateDatabaseUser = (data) => {
    return service({
        url: "/cmdb/databaseUser/updateDatabaseUser",
        method: 'put',
        data
    })
}


// @Tags Api
// @Summary 新增数据库用户信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateDatabaseUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /cmdb/databaseUser/CreateDatabaseUser [post]

export const createDatabaseUser = (data) => {
    return service({
        url: "/cmdb/databaseUser/createDatabaseUser",
        method: 'post',
        data
    })
}


// @Tags Api
// @Summary 删除数据用户信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteDatabaseUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/databaseUser/deleteDatabaseUser [post]
export const deleteDatabaseUser = (data) => {
    return service({
        url: "/cmdb/databaseUser/deleteDatabaseUser",
        method: 'delete',
        data
    })
}

// @Tags Api
// @Summary 根据Id查询数据库用户信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateDatabaseUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /cmdb/databaseUser/getDatabaseUserById[get]

export const getDatabaseUserById = (params) => {
    return service({
        url: "/cmdb/databaseUser/getDatabaseUserById",
        method: 'get',
        params
    })
}
