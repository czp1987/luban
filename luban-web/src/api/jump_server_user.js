import service from '@/utils/request'
// @Tags api
// @Summary 分页获取项目应用列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerUser.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /jump/user/getJumpServerUserList [Get]
// {
//  page     int
//	pageSize int
// }
export const getJumpServerUserList = (params) => {
    return service({
        url: "/jump/user/getJumpServerUserList",
        method: 'get',
        params
    })
}


// @Tags Api
// @Summary 添加堡垒机服务器
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateJumpServerUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /jump/user/createJumpServerUser [post]

export const createJumpServerUser = (data) => {
    return service({
        url: "/jump/user/createJumpServerUser",
        method: 'post',
        data
    })
}

// @Tags Api
// @Summary 添加堡垒机服务器别名
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.updateJumpServerUserParams true "更新服务器别名"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /jump/user/updateJumpServerUser [post]

export const updateJumpServerUser = (data) => {
    return service({
        url: "/jump/user/updateJumpServerUser",
        method: 'post',
        data
    })
}


// @Tags Api
// @Summary 删除项目应用信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteJumpServerUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/user/deleteJumpServerUser [delete]
export const deleteJumpServerUser = (data) => {
    return service({
        url: "/jump/user/deleteJumpServerUser",
        method: 'delete',
        data
    })
}


// @Tags Api
// @Summary 批量删除项目应用信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteJumpServerUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/user/deleteJumpServerUserByIds [delete]
export const deleteJumpServerUserByIds = (data) => {
    return service({
        url: "/jump/user/deleteJumpServerUserByIds",
        method: 'delete',
        data
    })
}


// @Tags Api
// @Summary 根据Id查询信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteJumpServerUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/user/getJumpServerUserById [get]
export const getJumpServerUserById = (params) => {
    return service({
        url: "/jump/user/getJumpServerUserById",
        method: 'get',
        params
    })
}
