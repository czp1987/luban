import service from '@/utils/request'
// @Tags api
// @Summary 分页获取项目应用列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.DeployApply.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/app/getDeployAppList [Get]
// {
//  page     int
//	pageSize int
// }
export const getDeployAppList = (params) => {
    return service({
        url: "/deploy/app/getDeployAppList",
        method: 'get',
        params
    })
}


// @Tags Api
// @Summary 更新项目应用信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.UpdateDeployApplyParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /deploy/app/updateDeployApp [post]
export const updateDeployApp = (data) => {
    return service({
        url: "/deploy/app/updateDeployApp",
        method: 'put',
        data
    })
}


// @Tags Api
// @Summary 新增项目应用信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.createDeployAppParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /deploy/app/createDeployApp [post]

export const createDeployApp = (data) => {
    return service({
        url: "/deploy/app/createDeployApp",
        method: 'post',
        data
    })
}



// @Tags Api
// @Summary 删除项目应用信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteDeployAppParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /deploy/app/deleteDeployApp [delete]
export const deleteDeployApp = (data) => {
    return service({
        url: "/deploy/app/deleteDeployApp",
        method: 'delete',
        data
    })
}


// @Tags Api
// @Summary 查询项目应用树信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.exportExcel true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /deploy/app/getDeployApplyTree [get]

export const getAppProjectAuthList = (data) => {
    return service({
        url: "/deploy/app/getAppProjectAuthList",
        method: 'post',
        data
    })
}
