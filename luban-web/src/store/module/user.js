import { login } from '@/api/user'
import { jsonInBlacklist } from '@/api/jwt'
import { getSysDictionaryList } from '@/api/sysDictionary'
import router from '@/router/index'
import { getPolicyProjectByAuthorityId } from '@/api/casbin'
export const user = {
    namespaced: true,
    state: {
        userInfo: {
            uuid: "",
            nickName: "",
            headerImg: "",
            authority: "",
        },
        token: "",
    },
    mutations: {
        setUserInfo(state, userInfo) {
            // 这里的 `state` 对象是模块的局部状态
            state.userInfo = userInfo
        },
        setToken(state, token) {
            // 这里的 `state` 对象是模块的局部状态
            state.token = token
        },
        LoginOut(state) {
            state.userInfo = {}
            state.token = ""
            router.push({ name: 'login', replace: true })
            sessionStorage.clear()
            window.location.reload()
        },
        ResetUserInfo(state, userInfo = {}) {
            state.userInfo = {...state.userInfo,
                ...userInfo
            }
        }
    },
    actions: {
        async LoginIn({ commit }, loginInfo) {
            const res = await login(loginInfo)
            if (res.code == 0) {
                commit('setUserInfo', res.data.user)
                commit('setToken', res.data.token)
                // 将字典 dictionaryList放入 localStorage
                const authorityIdData = await getPolicyProjectByAuthorityId({
                    authorityId: res.data.user.authorityId
                })
                let projectAuthData = {
                    authorityId: res.data.user.authorityId,
                    projectCasbinInfos: authorityIdData.data.projects
                }
                localStorage.setItem('projectAuth', JSON.stringify(projectAuthData))
                getSysDictionaryList({'page': 1, 'pageSize': 100}).then(
                  response => {
                      localStorage.setItem('dictionaryList', JSON.stringify(response.data.list))
                  }
                )
                const redirect = router.history.current.query.redirect
                if (redirect) {
                    router.push({ path: redirect })
                } else {
                    router.push({ path: '/layout/dashboard' })
                }
                return true
            }
        },
        async LoginOut({ commit }) {
            const res = await jsonInBlacklist()
            if (res.code == 0) {
                commit("LoginOut")

            }
        }
    },
    getters: {
        userInfo(state) {
            return state.userInfo
        },
        token(state) {
            return state.token
        },

    }
}
