import { store } from '@/store/index'
//  获取字典方法 使用示例 getDict('sex').then(res)  或者 async函数下 const res = await getDict('sex')
export const getDict = async (type) => {
    await store.dispatch("dictionary/getDictionary", type)
    return store.getters["dictionary/getDictionary"][type]
}

// 回显数据字典
export function selectDictLabel(datas, value) {
    var actions = []
    Object.keys(datas).map((key) => {
        if (datas[key].dictValue === ('' + value)) {
            actions.push(datas[key].dictLabel)
            return false
        }
    })
    return actions.join('')
}
