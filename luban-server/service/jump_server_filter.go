package service

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
)

//@author: heyibo
//@function: CreateJumpServerCmdFilter
//@description: 创建堡垒机过滤命令
//@param: data JumpServerUser
//@return: err error, data model.JumpServerCmdFilter

func CreateJumpServerCmdFilter(filter model.JumpServerCmdFilter)(err error)  {
	err = global.GVA_DB.Create(&filter).Error
	return err
}

//@author: heyibo
//@function: DeleteJumpServerCmdFilter
//@description: 删除堡垒机过滤命令
//@param: id ,jumpUser model.JumpServerCmdFilter
//@return: err error
func DeleteJumpServerCmdFilter(id float64) (err error) {
	var filter model.JumpServerCmdFilter
	db := global.GVA_DB.Where("id = ?", id).First(&filter)
	err = db.Delete(&filter).Error
	return err
}


//@author: heyibo
//@function: GetJumpServerCmdFilterInfoList
//@description: 分页获取数据
//@param: info request.PageInfo
//@return: err error, list interface{}, total int64

func GetJumpServerCmdFilterInfoList(info request.SearchJumpServerCmdFilterParams) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB.Model(&model.JumpServerCmdFilter{})
	var List []model.JumpServerCmdFilter

	if info.Command != "" {
		db = db.Where("command LIKE ?", "%"+info.Command+"%")
	}
	err = db.Count(&total).Error

	if err != nil {
		return err, List, total
	} else {
		err = db.Limit(limit).Offset(offset).Find(&List).Error
	}
	return err, List, total
}

//@author: heyibo
//@function: UpdateJumpServerCmdFilter
//@description: 更改过滤命令
//@param: deployApply model.JumpServerCmdFilter
//@return:err error, filter model.JumpServerCmdFilter

func UpdateJumpServerCmdFilter(filter model.JumpServerCmdFilter) (err error) {
	// 通过唯一ID进行更新
	err = global.GVA_DB.Where("id = ?", filter.ID).First(&model.JumpServerCmdFilter{}).Updates(&filter).Error
	return err
}



//@author: heyibo
//@function: Create
//@description: 更改过滤命令
//@param: deployApply model.JumpServerCmdFilter
//@return:err error, filter model.JumpServerCmdFilter
//func CreateMustSshFilterGroup(user string)(filterGroup *model.SshFilterGroup)  {
//	filterGroup =&model.SshFilterGroup{}
//    filterGroup.User = user
//    var list []model.JumpServerCmdFilter
//    err := global.GVA_DB.Find(&list).Error
//    if err !=nil {
//    	return nil
//	}
//	filterGroup.Filters = list
//	return
//}