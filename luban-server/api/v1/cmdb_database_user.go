package v1


import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags CMDBDatabaseUser
// @msp 创建数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.DatabaseUserForm true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /cmdb/database/createDatabaseUser [post]
func CreateDatabaseUser(c *gin.Context) {
	var R request.DatabaseUserForm
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	databaseUser := model.CMDBDatabaseUser{ClusterName: R.ClusterName,DatabaseName: R.DatabaseName,UserName: R.UserName,Password: R.Password,Permission: R.Permission,
		Status: R.Status,ProjectCode: R.ProjectCode,
	}
	if err := service.CreateDatabaseUser(databaseUser); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags CMDBDatabaseUser
// @Summary 删除数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/database/deleteDatabase [delete]
func DeleteDatabaseUser(c *gin.Context) {
	var Ids request.GetById
	_ = c.ShouldBindJSON(&Ids)
	if code,msg := utils.Validate(&Ids );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteDatabaseUser(Ids.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}


// @Tags CMDBDatabaseUser
// @Summary 更新数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBDatabaseUser true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/database/updateDatabaseUser [put]
func UpdateDatabaseUser(c *gin.Context) {
	var databaseUser model.CMDBDatabaseUser
	_ = c.ShouldBindJSON(&databaseUser)
	if err := service.UpdateDatabaseUser(databaseUser); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败"+ err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags CMDBDatabaseUser
// @Summary 查找ProjectList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchDatabaseUserParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/database/getDatabaseUserList [get]
func GetDatabaseUserList(c *gin.Context) {
	var pageInfo request.SearchDatabaseUserParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetDatabaseUserInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}



// @Tags CMDBDatabaseUser
// @Summary 通过ID查找CMDBDatabaseUser
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据id获取"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/database/getDatabaseUserById [post]
func GetDatabaseUserById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBindJSON(&reqId)
	if code, msg := utils.Validate(&reqId); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, results := service.FindDatabaseUserById(reqId.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(gin.H{"databaseList": results}, "获取数据成功!", c)
	}

}




