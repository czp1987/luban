package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags SysDepartment
// @heyibo 创建公司部门
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysDepartment true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /station/createDepartment [post]
func CreateDepartment(c *gin.Context) {
	var R request.DepartForm
	_ = c.ShouldBindJSON(&R)
	if code, msg := utils.Validate(&R); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	depart := model.SysDepartment{DeptCode: R.DeptCode, DeptName: R.DeptName, Status: R.Status, Remark: R.Remark, CompanyCode: R.CompanyCode}
	if err := service.CreateDepartment(depart); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败"+err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags SysDepartment
// @Summary 删除SysDepartment
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysDepartment true "SysDepartment模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /sysDictionary/deleteDepartment [delete]
func DeleteDepartment(c *gin.Context) {
	var dept model.SysDepartment
	_ = c.ShouldBind(&dept)
	if err := service.DeleteDepartment(&dept); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("有用户或岗位正在使用，删除失败！", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags SysDepartment
// @Summary 更新SysDepartment
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysDepartment true "SysDepartment模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /sysDictionary/updateDepartment [put]
func UpdateDepartment(c *gin.Context) {
	var R model.SysDepartment
	_ = c.ShouldBindJSON(&R)
	if code, msg := utils.Validate(&R); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err := service.UpdateDepartment(R); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags SysDepartment
// @Summary 分页获取SysDepartment列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SysDepartmentSearch true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /sysDictionary/getDepartmentList [get]
func GetDepartmentList(c *gin.Context) {
	var pageInfo request.SysDepartmentSearch
	_ = c.ShouldBind(&pageInfo)
	if code, msg := utils.Validate(&pageInfo); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, list, total := service.GetDepartmentInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

// @Tags SysDepartment
// @Summary 更新SysDepartment
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param Id body model.SysDepartment true "SysDepartment模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /department/GetDepartmentById [get]

func GetDepartmentById(c *gin.Context) {
	var idInfo request.GetById
	_ = c.ShouldBind(&idInfo)
	if err := utils.Verify(idInfo, utils.IdVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, results := service.GetDepartmentById(idInfo.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithDetailed(response.SysDepartmentResponse{Department: results}, "获取数据失败!", c)
	} else {
		response.OkWithDetailed(response.SysDepartmentResponse{Department: results}, "获取数据成功!", c)
	}
}

// @Tags SysDepartment
// @Summary 更新SysDepartment
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param companyCode body model.SysDepartment true "SysDepartment模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /sysDictionary/getDepartmentById [get]

func GetDepartmentByCode(c *gin.Context) {
	var R request.GetCompanyCode
	_ = c.ShouldBind(&R)
	if code, msg := utils.Validate(&R); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, results := service.FindDepartmentByCode(R.CompanyCode); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(gin.H{"deptList": results}, "获取数据成功!", c)
	}
}
