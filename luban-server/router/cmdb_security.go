package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)



func InitCMDBSUCURITY(Router *gin.RouterGroup) {
	CMDBSeurityRouter := Router.Group("security").Use(middleware.OperationRecord())
	{
		CMDBSeurityRouter.POST("createSecurity", v1.CreateSecurity)   // 添加Security项目
		CMDBSeurityRouter.PUT("updateSecurity", v1.UpdateSecurity)   // 更新Security项目
		CMDBSeurityRouter.GET("getSecurityList", v1.GetSecurityList)  // 获取Security信息
		CMDBSeurityRouter.DELETE("deleteSecurity", v1.DeleteSecurity) // 删除Security


	}
}

