package router

import (
	"gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitSysDictionaryDetailRouter(Router *gin.RouterGroup) {
	SysDictionaryDetailRouter := Router.Group("dictDetail").Use(middleware.OperationRecord())
	{
		SysDictionaryDetailRouter.POST("createDictionaryDetail", v1.CreateDictionaryDetail)   // 新建SysDictionaryDetail
		SysDictionaryDetailRouter.DELETE("deleteDictionaryDetail", v1.DeleteDictionaryDetail) // 删除SysDictionaryDetail
		SysDictionaryDetailRouter.PUT("updateDictionaryDetail", v1.UpdateDictionaryDetail)    // 更新SysDictionaryDetail
		SysDictionaryDetailRouter.GET("findDictionaryDetail", v1.FindDictionaryDetailById)     // 根据字典code获取SysDictionaryDetail
		SysDictionaryDetailRouter.GET("getDictionaryDetailList", v1.GetDictionaryDetailList)  // 获取SysDictionaryDetail列表
	}
}
