package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitCMDBDatabaseRouter(Router *gin.RouterGroup) {
	CMDBDatabaseRouter := Router.Group("database").Use(middleware.OperationRecord())
	{
		CMDBDatabaseRouter.POST("createDatabase", v1.CreateDatabase)   // 添加CMDB数据库信息
		CMDBDatabaseRouter.PUT("updateDatabase", v1.UpdateDatabase)   // 更新CMDB数据库信息
		CMDBDatabaseRouter.GET("getDatabaseById", v1.GetDatabaseById)  // 通过ID获取数据库信息
		CMDBDatabaseRouter.DELETE("deleteDatabase", v1.DeleteDatabase)  // 通过ID删除数据库
		CMDBDatabaseRouter.GET("getDatabaseList", v1.GetDatabaseList) // 获取数据库列表
		CMDBDatabaseRouter.GET("exportExcel", v1.DatabaseExportExcel)     // 导出excel列表

	}
}
