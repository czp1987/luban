package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitCMDBSERVER(Router *gin.RouterGroup) {
	CMDBServerRouter := Router.Group("server").Use(middleware.OperationRecord())
	{
		CMDBServerRouter.POST("createServer", v1.CreateServer)   // 添加CMDB项目
		CMDBServerRouter.PUT("updateServer", v1.UpdateServer)   // 更新CMDB项目
		CMDBServerRouter.PUT("updateServerByIdAndStatus",v1.UpdateServerByIdAndStatus) //更新服务器状态
		CMDBServerRouter.GET("getServerList", v1.GetServerList)  // 获取虚拟主机信息
		CMDBServerRouter.POST("getServersByProjectsList",v1.GetServersByProjectsList) //根据权限查询服务器
		CMDBServerRouter.POST("getServerById", v1.GetServerById)     // 根据ID 获取CmdbServers
		CMDBServerRouter.DELETE("deleteServer", v1.DeleteServer) // 删除CmdbServers
		CMDBServerRouter.GET("exportExcel", v1.ServerExportExcel) // 删除CmdbServers
	}
}
