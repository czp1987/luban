package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitSysLoginLogsRouter(Router *gin.RouterGroup) {
	SysLoginLogsRouter := Router.Group("loginLogs").Use(middleware.OperationRecord())
	{
		SysLoginLogsRouter.DELETE("deleteLoginLogs", v1.DeleteLoginLogs)           // 删除SysLoginLogs
		SysLoginLogsRouter.DELETE("deleteLoginLogsByIds", v1.DeleteLoginLogsByIds) // 批量删除SysLoginLogs
		SysLoginLogsRouter.GET("getLoginLogsList", v1.GetLoginLogsList)            // 获取SysLoginLogs列表

	}
}
