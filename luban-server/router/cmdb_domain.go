package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitCMDBDomainRouter(Router *gin.RouterGroup) {
	CMDBDomainRouter := Router.Group("domain").Use(middleware.OperationRecord())
	{
		CMDBDomainRouter.POST("createDomain", v1.CreateDomain)   // 添加CMDB 域名
		CMDBDomainRouter.DELETE("deleteDomain", v1.DeleteDomain)  // 通过ID删除域名
		CMDBDomainRouter.PUT("updateDomain", v1.UpdateDomain)   // 更新CMDB 域名
		CMDBDomainRouter.POST("getDomainById", v1.GetDomainById)  // 通过ID获取域名信息
		CMDBDomainRouter.POST("getDomainList", v1.GetCMDBDomainList) // 获取域名列表
		CMDBDomainRouter.GET("exportExcel", v1.DomainExportExcel)     // 导出excel列表
	}
}


