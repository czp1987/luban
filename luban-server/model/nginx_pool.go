package model

import "gin-luban-server/global"

// 如果含有time.Time 请自行import time包
type NginxPool struct {
	global.GVA_MODEL
	PoolName  string  `json:"pool_name" form:"pool_name" gorm:"uniqueIndex;column:pool_name;comment:pool名称;type:string"`
	Policy  string    `json:"policy" form:"policy" gorm:"column:policy;type:string;comment:转发策略"`
	Keepalive  int `json:"keepalive" form:"keepalive" gorm:"column:keepalive;type:int;default:20"`
	CheckType  string `json:"check_type" form:"check_type" gorm:"column:check_type;type:string;default:tcp"`
	CheckInterval  int `json:"check_interval" form:"check_interval" gorm:"column:check_interval;type:int;default:3000"`
	CheckTimeout  int `json:"check_timeout" form:"check_timeout" gorm:"column:check_timeout;type:int;default:3000"`
}
