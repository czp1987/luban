// 自动生成模板SysDictionary
package model

import (
	"gin-luban-server/global"
)

// 如果含有time.Time 请自行import time包
type SysDictionary struct {
	global.GVA_MODEL
	DictCode             string                `json:"dict_code" from:"dict_code" gorm:"type:varchar(50);not null;comment:字典编码"`
	DictName             string                `json:"dict_name" from:"dict_name" gorm:"type:varchar(50);not null;comment:字典名"`
	DictDesc             string                `json:"dict_desc" from:"dict_desc" gorm:"type:varchar(50);not null;comment:字典备注"`
	Status               *bool                 `json:"status" form:"status" gorm:"column:status;comment:状态"`
	SysDictionaryDetails []SysDictionaryDetail `json:"sysDictionaryDetails" form:"sysDictionaryDetails" gorm:"foreignKey:DictCode;references:DictCode;comment:字典编码"`
}
