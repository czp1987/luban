package request


type SftpLsFrom struct {
	ID       float64  `json:"id" form:"id" validate:"required"`
	Uuid     string   `json:"uuid" form:"uuid" validate:"required"`
	DirPath  string   `json:"dirPath" form:"dirPath" validate:"required"`
}

type SftpDeleteFileFrom struct {
	ID       float64  `json:"id" form:"id" validate:"required"`
	Uuid     string   `json:"uuid" form:"uuid" validate:"required"`
	FilePath  string   `json:"filePath" form:"filePath" validate:"required"`
}

//type SftpUploadFileFrom struct {
//	ID       float64  `json:"id" form:"id" validate:"required"`
//	Uuid     string   `json:"uuid" form:"uuid" validate:"required"`
//	DesDir  string   `json:"desDir" form:"desDir" validate:"required"`
//}

//type SftpDownLoadFileFrom struct {
//	ID       float64  `json:"id" form:"id" validate:"required"`
//	Uuid     string   `json:"uuid" form:"uuid" validate:"required"`
//	FileName string   `json:"fileName" form:"fileName" validate:"required"`
//	FilePath  string   `json:"filePath" form:"filePath" validate:"required"`
//}