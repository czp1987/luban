package request

import "gin-luban-server/model"




type SearchRedisClusterParams struct {
	model.RedisCluster
	PageInfo
	Desc  bool   `json:"desc"`
}


// redis database分页条件查询及排序结构体
type SearchCMDBRedisDatabaseParams struct {
	model.RedisDatabaseRecord
	PageInfo
}


type ExcelRedis struct {
	FileName string   `json:"fileName" form:"fileName"`
	model.RedisCluster
}
