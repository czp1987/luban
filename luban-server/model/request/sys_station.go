package request

import "gin-luban-server/model"

type SysStationSearch struct{
	model.SysStation
	PageInfo
}

type StationForm struct {
	StationCode string `json:"station_code" form:"station_code" gorm:"comment:部门编码" validate:"required" label:"部门编码"`
	StationName string `json:"station_name" form:"station_name" gorm:"comment:部门名称" validate:"required" label:"部门名称"`
	Remark      string `json:"remark" form:"remark" gorm:"comment:描述"`
	Status      *bool  `json:"status" form:"status" gorm:"column:status;comment:状态" validate:"required"`
	CompanyCode string `json:"company_code" form:"company_code" gorm:"column:company_code;comment:关联标记" validate:"required"`
	DeptCode    string `json:"dept_code" form:"dept_code" gorm:"column:dept_code;comment:关联标记" validate:"required"`
}


type GetCompanyAndDeptCode struct {
	CompanyCode string `json:"companyCode" validate:"required"`
	DeptCode    string `json:"deptCode" validate:"required"`
}