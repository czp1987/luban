package request

import "gin-luban-server/model"

type SysBasicConfigureFrom struct {
	BasicUser        string       `json:"basic_user" form:"basic_user" validate:"required"`
	SshType          string       `json:"sshType" form:"sshType" validate:"required"`
	BasicPasswd      string       `json:"basic_passwd" form:"basic_passwd"  `
	SshKey           string       `json:"sshKey" form:"sshKey"`
	Purpose          string       `json:"purpose" form:"purpose" validate:"required" `
	ProxyHost        string       `json:"proxy_host" form:"proxy_host"`
	ProxyPort        string       `json:"proxy_port" form:"proxy_port"`
	BaseUrl          string       `json:"base_url" form:"base_url"`
	ServerIdc        string       `json:"server_idc" form:"server_idc"`
	Remark           string       `json:"remark" form:"remark" `
	Status           *bool        `json:"status" form:"status" validate:"required" `
}


type SysBasicConfigureSearch struct{
	model.SysBasicConfigure
	PageInfo
	Desc     bool   `json:"desc"`
}
