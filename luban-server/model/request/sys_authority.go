package request

import "gin-luban-server/model"

type SysAuthoritySearch struct{
	model.SysAuthority
	PageInfo
}

type AuthorityForm struct {
	AuthorityId     string         `json:"authorityId" form:"authorityId" validate:"required" label:"角色编码"`
	AuthorityName   string         `json:"authority_name" form:"authority_name" validate:"required" label:"角色名称"`
	Remark          string         `json:"remark"  form:"remark" validate:"required" label:"备注" `
	Status          *bool          `json:"status" form:"status" validate:"required" label:"状态"`
}

