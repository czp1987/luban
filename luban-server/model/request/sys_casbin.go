package request

// Casbin info structure
type CasbinInfo struct {
	Path   string `json:"path"`
	Method string `json:"method"`
}

// Casbin structure for input parameters
type CasbinInReceive struct {
	AuthorityId string       `json:"authorityId" `
	CasbinInfos []CasbinInfo `json:"casbinInfos"`
}

type ProjectCasbinInfo struct {
	ProjectCode  string  `json:"project_code"`
}
type ProjectCasbinInReceive struct {
	AuthorityId         string                 `json:"authorityId" form:"authorityId"`
	ProjectCasbinInfos []ProjectCasbinInfo     `json:"projectCasbinInfos" form:"projectCasbinInfos"`
	SearchKey	       string `json:"search_key"`   // 提供搜索
	PageInfo
}