package request

import "gin-luban-server/model"

type SysCompanySearch struct{
	model.SysCompany
	PageInfo
}
type CompanyForm struct {
	CompanyCode string `json:"company_code" form:"company_code" gorm:"column:company_code;comment:公司编码" validate:"required" label:"公司编码"`
	CompanyName string `json:"company_name" form:"company_name" gorm:"column:company_name;comment:公司名称" validate:"required" label:"公司编码"`
	Status      *bool  `json:"status" form:"status" gorm:"column:status;comment:状态"`
	Remark      string `json:"remark" form:"remark" gorm:"type:varchar(100);column:remark;comment:描述" `
}


