package request

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
)

type DeployAppFrom struct {
	global.GVA_MODEL
	AppsName      string  `json:"apps_name" form:"apps_name" validate:"required"`
	AppsType      string   `json:"apps_type" form:"apps_type" validate:"required" `
	GitlabUrl     string  `json:"gitlab_url" form:"gitlab_url" validate:"required"`
	BranchName    string  `json:"branch_name" form:"branch_name" validate:"required"`
	ProjectCode   string  `json:"project_code" form:"project_code" validate:"required" `
}

type SearchDeployAppParams struct {
	model.DeployApp
	PageInfo
}

