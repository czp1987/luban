package request

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
)

type DeployAppConfigureFrom struct {
	global.GVA_MODEL
	AppsName      string   `json:"apps_name" form:"apps_name" validate:"required"`
	ProjectCode   string   `json:"project_code" form:"project_code" validate:"required"`
	PackageName   string   `json:"package_name" form:"package_name" validate:"required"`
	AppsJobName   string   `json:"apps_job_name" form:"apps_job_name" validate:"required"`
	BranchName    string   `json:"branch_name" form:"branch_name" validate:"required"`
	AppsPort      string   `json:"apps_port" form:"apps_port"`
	HealthCheck   string   `json:"health_check" form:"health_check"`
	AppsType      string   `json:"apps_type" form:"apps_type" validate:"required" `
	DeployEnv     string   `json:"deploy_env" form:"deploy_env" validate:"required"`
	DeployPath    string   `json:"deploy_path" form:"deploy_path" `
	BuildPath     string   `json:"build_path" form:"build_path" validate:"required" `
	BuildRun      string   `json:"build_run" form:"build_run" validate:"required"`
	DeployRun     string   `json:"deploy_run" form:"deploy_run" `
	ServerIdc     string   `json:"server_idc" form:""server_idc" `
	VirtualCodeInfos  []VirtualCodeInfo `json:"virtualCodeInfos" `
}

type UpdateDeployAppConfigureFrom struct {
	 model.DeployAppConfigure
	 VirtualCodeInfos  []VirtualCodeInfo `json:"virtualCodeInfos" validate:"required"`
}
type DeployAppVirtualFrom struct {
	AppsJobName   string   `json:"apps_job_name" form:"apps_job_name"`
	VirtualCodeInfos  []VirtualCodeInfo `json:"virtualCodeInfos"`
}

type VirtualCodeInfo struct {
	VirtualCode string  `json:"virtual_code"`
}


type DeployAppCopyRequest struct {
	Id float64 `json:"id" form:"id" validate:"required"`
	DeployEnv     string   `json:"deploy_env" form:"deploy_env" validate:"required"`
}

type SearchDeployAppConfigureParams struct {
	model.DeployAppConfigure
	PageInfo
}