package request

import (
	"gin-luban-server/model"
)

type CmdbServerFrom struct {
	VirtualCode  string `json:"virtual_code" form:"virtual_code"  validate:"required"`
	VirtualName  string `json:"virtual_name" form:"virtual_name" validate:"required"`
	ServerIdc    string `json:"server_idc" form:"server_idc" validate:"required"`
	Ipaddress    string `json:"ip_address" form:"ip_address" validate:"required"`
	User         string `json:"user" form:"user" validate:"required" `
	Password     string `json:"password" form:"password" validate:"required" `
	Sshkey       string `json:"sshkey" form:"sshkey" `
	Port         string `json:"port" form:"port" validate:"required"`
	OsSystem     string `json:"os_system" form:"os_system"  validate:"required"`
	OsType       string `json:"os_type" form:"os_type" validate:"required"`
	EnvName      string `json:"env_name" form:"env_name" validate:"required"`
	CpuNum       string `json:"cpu_num" form:"cpu_num" validate:"required" `
	MemInfo      string `json:"mem_info" form:"mem_info" validate:"required"`
	ProjectCode  string `json:"project_code" form:"project_code" validate:"required"`
	DiskSpace    string `json:"disk_space" form:"disk_space" validate:"required"`
	XShellProxy  string `json:"x_shell_proxy" form:"x_shell_proxy" validate:"required" `
	CompanyCode  string `json:"company_code" form:"company_code" validate:"required"`
}

type CmdbServersUpdateByIdAndStatusFrom struct {
	ID       int  `json:"id" form:"id" validate:"required"`
	Status   string  `json:"status" form:"status" validate:"required"`
}

type CmdbServersSearch struct{
	model.CmdbServer
	PageInfo
	Desc     bool   `json:"desc"`
}

type CmdbServersByProjectsSearch struct{
	ProjectCasbinInfos []ProjectCasbinInfo     `json:"projectCasbinInfos" form:"projectCasbinInfos"`
	PageInfo
}

type ExcelServer struct {
	FileName string   `json:"fileName" form:"fileName"`
	model.CmdbServer
}
