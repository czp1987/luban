package model

import "gin-luban-server/global"

type DeployAppConfigure struct {
	global.GVA_MODEL
	AppsName      string   `json:"apps_name" form:"apps_name" gorm:"column:apps_name;comment:应用名称"`
	ProjectCode   string   `json:"project_code" form:"project_code" gorm:"column:project_code;comment:项目code"`
	PackageName   string   `json:"package_name" form:"package_name" gorm:"column:package_name;comment:应用包名称"`
	AppsJobName   string   `json:"apps_job_name" form:"apps_job_name" gorm:"column:apps_job_name;comment:应用jenkins名称"`
	BranchName    string   `json:"branch_name" form:"branch_name" gorm:"column:branch_name;comment:默认分支名称"`
	AppsPort      string   `json:"apps_port" form:"apps_port" gorm:"column:apps_port;comment:应用端口"`
	HealthCheck   string   `json:"health_check" form:"health_check" gorm:"column:health_check;comment:健康检查"`
	AppsType      string   `json:"apps_type" form:"apps_type" gorm:"column:apps_type;comment:应用类型"`
	DeployEnv     string   `json:"deploy_env" form:"deploy_env" gorm:"column:deploy_env;comment:部署环境"`
	DeployPath    string   `json:"deploy_path" form:"deploy_path" gorm:"column:deploy_path;comment:部署路径"`
	DeployStatus  string   `json:"deploy_status" form:"deploy_status" gorm:"column:deploy_status;comment:部署状态"`
	BuildPath     string   `json:"build_path" form:"build_path" gorm:"column:build_path;comment:编译路径"`
	BuildRun      string   `json:"build_run" form:"build_run" gorm:"column:build_run;comment:编译命令"`
	DeployRun     string   `json:"deploy_run" form:"deploy_run" gorm:"column:deploy_run;type:varchar(512);comment:部署命令"`
	ServerIdc     string   `json:"server_idc" form:""server_idc" gorm:"column:"server_idc;comment:部署机房"`
	DeployAppVirtual []DeployAppVirtual `json:"DeployAppVirtual" form:"DeployAppVirtual" gorm:"foreignKey:AppsJobName;references:AppsJobName;comment:应用jenkins名称"`
	AppsJenkins   DeployAppJenkins    `json:"appsJenkins" form:"appsJenkins" gorm:"foreignKey:AppsJobName;references:AppsJobName;comment:应用jenkins名称"`
	Projects      CMDBProject     `json:"projects" gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
}


type DeployAppVirtual struct {
	AppsJobName   string  `json:"apps_job_name" form:"apps_job_name" gorm:"column:apps_job_name;comment:应用部署code"`
	VirtualCode   string  `json:"virtual_code" form:"virtual_code" gorm:"column:virtual_code;comment:虚拟机code"`
}




