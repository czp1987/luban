package response

import "gin-luban-server/model"

type CMDBDVirtualResponse struct {
	Virtual []model.CmdbServer `json:"virtual"`
}
