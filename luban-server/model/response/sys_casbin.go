package response

import "gin-luban-server/model/request"

type PolicyPathResponse struct {
	Paths []request.CasbinInfo `json:"paths"`
}
type PolicyProjectResponse struct {
	Projects []request.ProjectCasbinInfo `json:"projects"`
}
