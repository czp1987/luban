package datas

import (
	"github.com/gookit/color"
	"gorm.io/gorm"
	"os"
)

type SysAuthorityMenus struct {
	SysAuthorityAuthorityId string
	SysBaseMenuId           uint
}

var AuthorityMenus = []SysAuthorityMenus{
	{"admin", 1},
	{"admin", 2},
	{"admin", 3},
	{"admin", 4},
	{"admin", 5},
	{"admin", 6},
	{"admin", 7},
	{"admin", 8},
	{"admin", 9},
	{"admin", 10},
	{"admin", 11},
	{"admin", 12},
	{"admin", 13},
	{"admin", 14},
	{"admin", 15},
	{"admin", 16},
	{"admin", 17},
	{"admin", 18},
	{"admin", 19},
	{"admin", 20},
	{"admin", 21},
	{"admin", 22},
	{"admin", 23},
	{"admin", 24},
	{"admin", 25},
	{"admin", 26},
	{"admin", 27},
	{"admin", 28},
	{"admin", 29},
	{"admin", 30},
	{"admin", 31},
	{"admin", 32},
	{"admin", 33},
	{"admin", 34},
	{"admin", 35},
	{"admin", 36},
	{"admin", 37},
	{"admin", 38},
	{"admin", 39},
	{"admin", 40},
	{"admin", 41},
	{"admin", 42},
	{"admin", 43},
	{"admin", 44},
	{"admin", 45},
	{"admin", 46},
	{"admin", 47},
	{"admin", 48},
	{"admin", 49},
	{"admin", 50},


}

func InitSysAuthorityMenus(db *gorm.DB) {
	if err := db.Table("sys_authority_menus").Transaction(func(tx *gorm.DB) error {
		if tx.Where("sys_authority_authority_id IN ?", []string{"admin"}).Find(&[]SysAuthorityMenus{}).RowsAffected == 53 {
			color.Danger.Println("sys_authority_menus表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&AuthorityMenus).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_authority_menus 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
