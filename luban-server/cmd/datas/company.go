package datas

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"github.com/gookit/color"
	"gorm.io/gorm"
	"os"
	"time"
)

func InitSysCompany(db *gorm.DB) {
	status := new(bool)
	*status = true
	Company := []model.SysCompany{
		{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()},CompanyCode: "100001",CompanyName: "安吉加加信息技术有限公司",Status: status,Remark: "安吉加加"},
	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1}).Find(&[]model.SysCompany{}).RowsAffected == 1 {
			color.Danger.Println("sys_company表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Company).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_company 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
