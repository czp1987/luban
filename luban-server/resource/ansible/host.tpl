[initHost]
{{ .Ipaddress}}  hostname={{.VirtualName}}  localIp={{.Ipaddress}}  ansible_ssh_user={{.User}}  ansible_ssh_pass={{.Password}} env={{.EnvName}}  server_idc={{.ServerIdc}}
