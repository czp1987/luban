---
home: true
heroImage: /logo.png
actionText: 快速开始 →
actionLink: /guide/
footer: Apache 2.0 Licensed | Copyright © 2021 Anji-Plus Report All Rights Reserved
---

<div style="text-align: center">
</div>

<div class="features">
  <div class="feature">
    <h2>技术先进</h2>
    <p>使用最流行的技术golang、Vue、Element。</p>
  </div>
  <div class="feature">
    <h2>丰富组件</h2>
    <p>是一个包含前后端代码实现，同时支持动态扩展机房.</p>
  </div>
  <div class="feature">
    <h2>功能完善</h2>
    <p>支持多机房，多云，实现夸机房管理</p>
  </div>
</div>
