## 应用配置

点击项目可新增应用

![img_3.png](../picture/dashboard/img.png) <br>
![img_3.png](../picture/dashboard/img_1.png) <br>
![img_3.png](../picture/dashboard/img_2.png) <br>

## 应用部署

点击项目对需要的应用进行部署

![img_3.png](../picture/dashboard/img_3.png) <br>
![img_4.png](../picture/dashboard/img_4.png) <br>

## 应用回滚

选择需要回滚的应用，调到想要回滚到的版本进行应用回滚

![img_5.png](../picture/dashboard/img_5.png) <br>

## 应用重启

选择需要重启的应用、环境，点击开始重启即可对需要的应用进行重启。

![img1](../picture/charts/img.png) <br>

## 发布历史

可查看构建日志

![img1](../picture/charts/img_1.png) <br>
![img1](../picture/charts/img_2.png) <br>
