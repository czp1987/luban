## 新增用户

![img](../picture/authmanager/img.png) <br>

![img](../picture/authmanager/img_1.png) <br>

## 用户授权

**注意**：新建用户完成后需要给用户授权，否则新用户登陆是啥也看不到。<br>
![img](../picture/authmanager/img_3.png) <br>

![img](../picture/authmanager/img_4.png) <br>
**注**：这里没有给新用户赋予默认角色的原因是，在角色管理中角色是可以被删除和修改的，因此在新建用户时需要手动的去授权角色 <br>




