## 项目管理

- 新增项目<br>

![source.png](../picture/datasource/img_1.png)
![source.png](../picture/datasource/img_2.png)
## 虚拟机资源

- 新增服务器 <br>

![img2](../picture/datasource/img_3.png)  <br>
![img3](../picture/datasource/img_4.png)  <br>

## 数据库管理

- 新增数据库

![An image](../picture/datasource/img_5.png)<br>
![An image](../picture/datasource/img_6.png)<br>

## 域名管理

- 新增域名

![An image](../picture/datasource/img_7.png)<br>
![An image](../picture/datasource/img_8.png)<br>

## Redis资源

- 新增集群

![An image](../picture/datasource/img_9.png)<br>
![An image](../picture/datasource/img_10.png)<br>

- 查看库数

![An image](../picture/datasource/img_11.png)<br>
![An image](../picture/datasource/img_12.png)<br>



  
  
